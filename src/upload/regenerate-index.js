const { yhf } = require('../../env.json');
const { promisify: pr } = require('util');
const path = require('path');
const glob = require('glob');
const fs = require('fs/promises');
const mkdirp = require('mkdirp');
const { Feed } = require('feed');

module.exports = async function rebuildIndex() {
  let files = await pr(glob)(path.join(yhf.data_dir, 'content', '**/*.json'));
  let content = [];
  for (let file of files) {
    try {
      let json = JSON.parse(await fs.readFile(file, 'utf8'))
      content.push(json);
    } catch(ex) {
      console.warn("Error reading file", file, ex);
    }
  }
  await fs.writeFile(
    path.join(yhf.data_dir, 'data.json'),
    JSON.stringify(content, null, 2)
  );

  let feed = new Feed({
    title: "YHF@TETR.IO+: User content for TETR.IO",
    description: "User-generated content for TETR.IO. Skins, sound effects, music graphs and more.",
    id: "https://you.have.fail/ed/at/tetrioplus",
    link: "https://you.have.fail/ed/at/tetrioplus",
    language: "en",
    copyright: "Site © UniQMG 2020. Content owned by their respective authors.",
    updated: new Date(),
    feedLinks: {
      json: "https://you.have.fail/ed/at/tetrioplus/feed.json",
      atom: "https://you.have.fail/ed/at/tetrioplus/feed.atom",
    },
    author: {
      name: "UniQMG",
    }
  });
  content
    .sort(({ uploaded: a }, { uploaded: b }) => a < b ? 1 : -1)
    .slice(0, 30)
    .forEach(item => {
      feed.addItem({
        title: item.name + ' by ' + item.author,
        id: "content-" + item.id,
        link: 'https://you.have.fail/ed/at/tetrioplus/#' + item.id,
        description: item.description || "No description",
        content: [
          `New ${item.type} by ${item.author}: ${item.name}`
        ].filter(e => e).join('\n'),
        author: [{
          name: item.author,
          email: null,
          link: null
        }],
        contributor: [],
        date: new Date(item.uploaded),
        image: item.type == 'skin'
          ? `https://you.have.fail/ed/at/tetrioplus/${item.path}`
          : null
      });
    });

  await fs.writeFile(path.join(yhf.data_dir, 'feed.rss'), feed.rss2());
  await fs.writeFile(path.join(yhf.data_dir, 'atom.xml'), feed.atom1());
}

module.exports.getIndex = async function() {
  try {
    return JSON.parse(await fs.readFile(path.join(yhf.data_dir, 'data.json'), 'utf8'));
  } catch(ex) {
    await rebuildIndex();
    return JSON.parse(await fs.readFile(path.join(yhf.data_dir, 'data.json'), 'utf8'))
  }
}
