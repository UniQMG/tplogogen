function trnc(string, length=100) {
  if (string.length > length)
    return string.slice(0, length) + '...';
  return string;
}

module.exports.approved = function(content, tpse) {
  const embed = {};
  embed.title = "Content approved";
  embed.color = 0x635293;
  let url = `https://you.have.fail/ed/at/tetrioplus#${content.id}`;
  embed.description = `Your submission **${content.name}** (**[${content.id}](${url})**) has been approved`;
  if (content.quality != 'normal')
    embed.description += ` and marked as *${content.quality}* quality`;
  if (content.visibility == 'author')
    embed.description += ` but is visible on your author page only`
  embed.description += '.';
  return embed;
}
module.exports.denied = function() {
  const embed = {};
  embed.title = "Submission denied";
  embed.color = 0xFF0000;
  embed.description = (
    `Your submission has been denied. ` +
    `Depending on the reason given, you may be able to resubmit it with some ` +
    `changes or otherwise appeal this decision.`
  );
  return embed;
}
module.exports.processing = function(logs) {
  const embed = {};
  embed.title = "Content submission";
  embed.color = 0x635293;
  embed.description = "Processing...";
  if (logs) embed.description += "```" + logs + "```";
  return embed;
}
module.exports.error = function(ex) {
  const embed = {};
  embed.title = "Submission error";
  embed.color = 0xFF0000;
  embed.description = `Error importing submission: ${ex}`;
  console.error("Submission error", ex);
  return embed;
}
module.exports.pending = function(content, tpse, options) {
  const embed = { fields: [] };

  embed.description = `Future ID: **${content.id}**`;
  if (tpse) {
    embed.title = "Content submission";
    embed.color = 0x7f23b8;
    let type =
    embed.description += (
      `\nDetermined import type: \`${content.type}/${content.format}\``
    )
  } else {
    embed.title = "Content submission: Automatic import failed";
    embed.description += (
      "\nYou may still be able to submit this content, but it won't have an " +
      "automatic preview on the site or be usable in-game. Please try to " +
      "correct import errors. You can verify your content imports correctly " +
      "by drag-and-dropping it on to the main TETR.IO PLUS window or " +
      "data management window."
    );
    embed.color = 0xFF0000;
  }

  if (options?.tpseKeys) {
    let keys = options.tpseKeys.map(e => `"${e}"`).join(', ').replace(/`/g, '\\`');
    if (keys.length > 512) {
      keys = `${keys.slice(0, 512)} ... (${options.tpseKeys.length} keys)`;
    }
    embed.fields.push({
      name: 'TPSE file',
      value: `Keys present: \`\`\`[${keys}]\`\`\``
    });
  }

  if (options?.previewRecommended) {
    embed.fields.push({
      name: `⚠️ No custom preview set for \`${content.type}\` content type`,
      value: (
        `This content's type can't be determined or resolved to a single type. ` +
        `It won't have a preview on the site, so you should supply one with the ` +
        `\`rv!setpreview\` command.`
      )
    });
  }

  if (options?.tpseWarning) {
    embed.fields.push({
      name: '⚠️ Uploading TPSE files is not recommended',
      value: (
        'TPSE files cannot be automatically previewed or categorized. ' +
        'Use them only as a last resort or as a dedicated content pack. ' +
        'Prefer `zip`s of content, especially for sound effects.'
      )
    });
  }

  if (options?.exists) {
    embed.fields.push({
      name: '⚠️ Content already exists',
      value: 'You will be replacing a previous submission.'
    });
  }

  if (options?.warnings) {
    let warningText = options.warnings.slice(0, 10).join('\n');
    if (options.warnings.length > 10)
      warningText += `\n(${options.warnings.length} more warnings...)`
    embed.fields.push({
      name: '⚠️ Import warnings',
      value: warningText || "?"
    });
  }

  if (options?.override) {
    embed.fields.push({
      name: `ℹ️ Reviewer overrides`,
      value: (
        "A reviewer has overriden some submission options" +
        "```" + options.override + "```"
      )
    });
  }

  if (options?.attachmentMessage) {
    embed.fields.push({
      name: 'ℹ️ Attachment replaced',
      value: `The uploader has replaced the file with [this one](${options.attachmentMessage.url})`
    });
  }

  if (options?.previewMessage) {
    embed.fields.push({
      name: 'ℹ️ Custom preview',
      value: `The uploader set custom preview media [here](${options.previewMessage.url})`
    });
  }

  let lines = [
    trnc(content.description, 500),
    `Author: **${trnc(content.author)}**`
  ];
  if (content.parent) {
    let yhflink = `https://you.have.fail/ed/at/tetrioplus/#${content.parent}`;
    let keyword = content.derivative ? '**Derivative**' : 'Variant';
    lines.push(`${keyword} of [${trnc(content.parent)}](${yhflink})`);
  }

  embed.fields.push({
    name: `${trnc(content.name)}`,
    value: lines.join('\n')
  });

  return embed;
}
