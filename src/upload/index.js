const regenerateIndex = require('./regenerate-index');
const performImport = require('./perform-import');
const { yhf } = require('../../env.json');
const { promisify: pr } = require('util');
const fetch = require('node-fetch');
const embeds = require('./embeds');
const mkdirp = require('mkdirp');
const level = require('level');
const fs = require('fs');
const fsp = require('fs/promises');
const path = require('path');

const submissions = level('./db/submissions'); // msg -> response
const responses = level('./db/responses'); // response -> msg

const { makeLogger, download, get, packageAttachments } = require('./util.js');

module.exports.delete = async function(msg) {
  let submission = await get(submissions, msg.id);
  if (!submission) return;
  if (submission.thread) {
    let thread = await msg.channel.threads.fetch(submission.thread);
    await thread.send("\\⚠️ Submission deleted").catch(ex => _);
  }
  await msg.channel.messages
    .fetch(submission.response)
    .then(res => res.delete())
    .catch(ex => null);
  await responses.del(submission.response).catch(ex => null);
  await submissions.del(msg.id);
}

async function assertReviewer(msg) {
  if (!msg.member.roles.resolve(yhf.reviewer))
    throw 'Reviewer only command';
}
async function submissionFromThread(msg) {
  if (!msg.channel.isThread())
    throw 'You must use this command in a submission thread';

  let response = await get(responses, msg.channel.id);
  if (!response) throw 'You must use this command in a submission thread';


  let submissionMsg = await msg.channel.parent.messages.fetch(response.submission);
  if (!submissionMsg) throw "Can't find submission message";

  let submission =  await get(submissions, response.submission);
  return { response, submission, submissionMsg };
}


module.exports.refreshSubmission = async function(msg) {
  await assertReviewer(msg);
  let { response, submission, submissionMsg } = await submissionFromThread(msg);
  await module.exports.message(submissionMsg);
  return "submission refreshed";
}
module.exports.reopenSubmission = async function(msg) {
  await assertReviewer(msg);
  let { response, submission, submissionMsg } = await submissionFromThread(msg);
  submission.status = 'pending';
  await submissions.put(response.submission, JSON.stringify(submission));
  await module.exports.message(submissionMsg);
  return "submission reopened";
}
module.exports.overrideArguments = async function(msg, ...args) {
  await assertReviewer(msg);
  let { response, submission, submissionMsg } = await submissionFromThread(msg);
  submission.override = msg.content.replace(/^\S+\s+/, '');
  await submissions.put(response.submission, JSON.stringify(submission));
  await module.exports.message(submissionMsg);
  return "Submission overrides set: ```" + submission.override + "```";
}
module.exports.bumpThreads = async function(channel) {
  await new Promise((res, rej) => {
    submissions.createReadStream()
      .on('data', ({ key, value: json }) => {
        let submission = JSON.parse(json);
        if (submission.status != 'pending') return;
        channel.threads.fetch(submission.thread).then(thread => {
          if (!thread.archived) return;
          return thread.send(`<@&${yhf.reviewer}> this submission is still pending.`);
        }).catch(ex => {
          console.warn('Failed to bump thread:', ex)
        });
      })
      .on('error', ex => rej(ex))
      .on('end', () => res());
  });
}

function submissionAddFile(callback) {
  return async function(msg) {
    if (!msg.channel.isThread())
      throw 'You must use this command in a submission thread';

    let response = await get(responses, msg.channel.id);
    if (!response) throw 'You must use this command in a submission thread';

    let submission = await get(submissions, response.submission);
    if (submission.status != 'pending')
      throw "This submission isn't pending";

    let submissionMsg = await msg.channel.parent.messages.fetch(response.submission);
    if (submissionMsg.author.id != msg.author.id)
      throw "This isn't your submission thread";

    if (!msg.attachments.first())
      throw 'This command requires an attached file';

    if (await callback(msg, submission) !== false)
      await submissions.put(response.submission, JSON.stringify(submission));

    await module.exports.message(submissionMsg);
    return "Attachment updated";
  }
}
module.exports.setSubmissionAttachmentMessage = submissionAddFile((msg, submission) => {
  submission.attachmentMessage = msg.id;
  return true;
});
module.exports.setSubmissionPreview = submissionAddFile((msg, submission) => {
  submission.previewMessage = msg.id;
  return true;
});

async function compileSubmissionInfo(msg) {
  const data = {
    message: msg,
    submission: null,
    thread: null,
    contentMetadata: null,
    messages: {
      response: null,
      attachment: msg,
      preview: null
    }
  };

  data.submission = await submissions.get(msg.id)
    .then(msg => JSON.parse(msg))
    .catch(ex => null);

  if (data.submission) {
    data.thread = await msg.channel.threads
      .fetch(data.submission.thread)
      .catch(_ex => null);

    if (data.submission.contentPath) {
      try {
        const jsonpath = path.join(yhf.data_dir, data.submission.contentPath) + '.json';
        data.contentMetadata = JSON.parse(await fsp.readFile(jsonpath, 'utf8'));
      } catch(ex) { console.warn("Failed loading existing submission data", ex); }
    }

    if (data.submission.attachmentMessage != data.messages.attachment) {
      data.messages.attachment = await Promise.resolve(data.thread)
        .then(thread => thread.messages.fetch(data.submission.attachmentMessage))
        .catch(_ex => data.messages.attachment);
    }

    if (data.submission.previewMessage) {
      data.messages.preview = await Promise.resolve(data.thread)
        .then(thread => thread.messages.fetch(data.submission.previewMessage))
        .catch(_ex => null);
    }

    data.messages.response = await msg.channel.messages
      .fetch(data.submission.response)
      .catch(_ex => null);
  }

  return data;
}
async function getSubmissionFromIndex(id) {
  let data = JSON.parse(await fsp.readFile(path.join(yhf.data_dir, 'data.json')));
  return data.filter(data => data.id == id)[0];
}
module.exports.reaction = async function(msg, reaction, member) {
  if (member.id == msg.client.user.id) return;
  if (msg.partial) await msg.fetch();

  let responseInfo = await get(responses, msg.id);
  if (!responseInfo) return;
  let submissionMsg = await msg.channel.messages.fetch(responseInfo.submission);
  let info = await compileSubmissionInfo(submissionMsg);
  if (!info.messages.response || !info.submission) return;

  if (info.submission.status != 'pending') {
    await msg.reactions.removeAll();
    return;
  }

  if (!member.roles.resolve(yhf.reviewer)) {
    await reaction.users.remove(member.id);
    return;
  }

  let unlocked = (await msg.reactions.resolve('🔓')?.users?.fetch())?.has(member.user.id);
  let locked = (await msg.reactions.resolve('🔒')?.users?.fetch())?.size > 1;

  switch (reaction.emoji.name) {
    case '🇦': break;
    case '🔒': break;
    case '🔓': break;
    case '❌':
      if (locked && !unlocked) {
        await reaction.users.remove(member.id);
        break;
      }
      info.submission.status = 'denied';
      await submissions.put(responseInfo.submission, JSON.stringify(info.submission));
      await msg.edit({ embeds: [embeds.denied()] });
      await msg.reactions.removeAll();
      break;
    case '✅':
      let logger = makeLogger(msg);
      try {
        if (locked && !unlocked) {
          await reaction.users.remove(member.id);
          break;
        }
        let authorOnly = (await msg.reactions.resolve('🇦')?.users?.fetch())?.has(member.user.id);
        await msg.edit({ embeds: [embeds.processing()] });

        const { content, tpse, warnings, error } = await performImport({
          message: info.message,
          argumentOverrides: info.submission.override,
          file: await packageAttachments(info.messages.attachment.attachments),
          previewName: info.messages.preview?.attachments.first().name,
          log: logger.log
        }).catch(ex => ({ error: ex }));
        if (error) console.error(error);
        if (!content) throw "Import failed";
        content.visibility = authorOnly ? 'author' : 'public';

        logger.log("Saving attachment...");

        console.log(info.messages.attachment.attachments, yhf.data_dir, content.path)
        await Promise.all([
          download(
            'attachment',
            await packageAttachments(info.messages.attachment.attachments, true),
            path.join(yhf.data_dir, content.path)
          ),
          info.messages.preview && download(
            'preview',
            info.messages.preview.attachments.first().url,
            path.join(yhf.data_dir, content.previewPath)
          )
        ]);
        logger.log("Done");

        // If there's already a submission sharing this ID, use it instead of
        // whatever is stored in the submission database
        // (Data on disk is always authoritative)
        let altMeta = await getSubmissionFromIndex(content.id);
        if (altMeta)
          info.contentMetadata = altMeta;

        const pathsToDelete = [];
        if (info.contentMetadata) {
          if (info.contentMetadata.id)
            content.id = info.contentMetadata.id;

          if (info.contentMetadata.uploaded)
            content.uploaded = info.contentMetadata.uploaded;

          if (info.contentMetadata.path != content.path) {
            pathsToDelete.push(info.contentMetadata.path);
            pathsToDelete.push(info.contentMetadata.path + '.json');
          }

          if (info.contentMetadata.tpsePath != content.tpsePath)
            pathsToDelete.push(info.contentMetadata.tpsePath);

          if (info.contentMetadata.previewPath != content.previewPath)
            pathsToDelete.push(info.contentMetadata.previewPath);
        }

        logger.log("Writing metadata...");
        const jsonpath = path.join(yhf.data_dir, content.path) + '.json';
        await fsp.writeFile(jsonpath, JSON.stringify(content, null, 2));
        if (tpse) {
          const tpsepath = path.join(yhf.data_dir, content.tpsePath);
          await mkdirp(tpsepath.replace(/\\/g, '/').split('/').slice(0, -1).join('/'));
          await fsp.writeFile(tpsepath, JSON.stringify(tpse));
        }

        if (pathsToDelete.length > 0) {
          logger.log("Removing old files...");
          for (let subpath of pathsToDelete) {
            if (!subpath) continue;
            logger.log(path.join(yhf.data_dir, subpath));
            await fsp.unlink(path.join(yhf.data_dir, subpath));
          }
        }

        logger.log("Regenerating index...");
        await regenerateIndex();
        await logger.stop();

        info.submission.status = 'approved';
        info.submission.contentPath = content.path;
        await submissions.put(responseInfo.submission, JSON.stringify(info.submission));
        await msg.edit({ embeds: [embeds.approved(content, tpse)] });
        await msg.reactions.removeAll();
      } catch(ex) {
        ex.trace();
        await logger.stop();
        await msg.edit({ embeds: [embeds.error(ex)] });
      } finally {
        break;
      }
    default: await reaction.remove();
  }
}
module.exports.message = async function(msg) {
  if (msg.author.id == msg.client.user.id) return;
  if (msg.channel.isThread()) return;

  let info = await compileSubmissionInfo(msg);

  if (info.submission && info.submission.status != 'pending')
    return false;

  if (!info.messages.attachment.attachments.first()) {
    if (!msg.member.roles.resolve(yhf.reviewer))
      await msg.delete();
    return;
  }

  if (info.messages.response) {
    await info.messages.response.edit({ embeds: [embeds.processing()] })
  } else {
    info.messages.response = await msg.reply({ embeds: [embeds.processing()] });
  }

  let logger = makeLogger(info.messages.response);
  try {
    if (!info.thread) {
      info.thread = await msg.channel.threads.create({
        name: `Discussion`,
        startMessage: info.messages.response
      });
    }
    await submissions.put(msg.id, JSON.stringify({
      contentPath: info.submission?.contentPath || null,
      attachmentMessage: info.messages.attachment.id,
      previewMessage: info.messages.preview?.id || null,
      override: info.submission?.override || null,
      response: info.messages.response.id,
      status: 'pending',
      thread: info.thread.id
    }));
    await responses.put(info.messages.response.id, JSON.stringify({
      submission: msg.id
    }));
    await info.messages.response.reactions.removeAll();

    const { content, tpse, warnings } = await performImport({
      message: msg,
      argumentOverrides: info.submission?.override,
      file: await packageAttachments(info.messages.attachment.attachments),
      log: logger.log
    });
    await logger.stop();

    await info.thread.setName(`Discussion for ${content.name}`);

    let alreadyExists = false;
    if (info.contentMetadata) {
      content.id = info.contentMetadata.id;
      alreadyExists = true;
    }
    if (fs.existsSync(path.join(yhf.data_dir, content.path)))
      alreadyExists = true;
    if (await getSubmissionFromIndex(content.id))
      alreadyExists = true;

    let options = {
      exists: alreadyExists,
      override: info.submission?.override || null,
      previewRecommended: content.type == 'unknown' && !info.messages.preview,
      tpseKeys: content.path.endsWith('.tpse') ? Object.keys(tpse) : null,
      tpseWarning: content.path.endsWith('.tpse') && !tpse.musicGraph,
      attachmentMessage: (info.messages.attachment.id != msg.id) && info.messages.attachment,
      previewMessage: info.messages.preview,
      warnings: warnings
    };

    await info.messages.response.edit({
      embeds: [embeds.pending(content, tpse, options)]
    });
    await info.messages.response.react('🔒');
    await info.messages.response.react('🇦');
    await info.messages.response.react('❌');
    await info.messages.response.react('✅');
  } catch(ex) {
    await logger.stop();
    await info.messages.response.edit({
      embeds: [embeds.error(ex)]
    });
  } finally {
    return true;
  }
}
