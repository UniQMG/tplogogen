const { Image } = require('canvas');
const mime = require('mime-types');

module.exports = async function(name, buffer) {
  const mimetype = !name.endsWith('.tpse')
    ? mime.lookup(name)
    : 'application/json+tpse';
  if (!mimetype) throw new Error('Unknown file type');
  const data = `data:${mimetype};base64,` + buffer.toString('base64');

  const file = { name: name, type: mimetype, data: data };
  if (file.type.startsWith('image/')) {
    file.image = new Image();
    let pr = new Promise((res, rej) => {
      file.image.onload = res;
      file.image.onerror = ex => {
        rej(new Error('Failed to load image: ' + ex));
      }
    });
    file.image.src = file.data;
    await pr;
  }

  return file;
}
