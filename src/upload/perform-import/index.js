const sanitizeFilename = require('sanitize-filename');
const importFile = require('./import-file');
const parseArgs = require('./parse-args');
const { yhf } = require('../../../env.json');
const fsp = require('fs/promises');
const path = require('path');

module.exports = async function performImport({
  message: msg,
  argumentOverrides,
  file,
  previewName,
  log=(()=>{})
}) {
  const args = parseArgs(msg.content);
  if (argumentOverrides)
    Object.assign(args, parseArgs(argumentOverrides));

  const content = {
    version: 1,
    id: null,
    name: args.name || file.name.split('.').slice(0,-1).join('.'),
    author: args.author || msg.member.nickname || msg.author.username,
    description: args.description || "",
    visibility: 'public', // currently 'public' or 'author', assigned elsewhere.
    quality: 'normal', // low, medium, or high, assigned elsewhere.
    uploaded: Date.now(),
    updated: Date.now(),
    parent: args.variant || null,
    derivative: false,
    type: 'unknown',
    format: null,
    modified: [],
    tags: [],
    path: null,
    tpsePath: null,
    previewPath: null,
    source: msg.url,
    import: {}
  };

  try {
    if (content.parent) {
      let data = JSON.parse(await fsp.readFile(path.join(yhf.data_dir, 'data.json')));
      let parent = data.filter(ct => ct.id == content.parent)[0];
      if (!parent) throw new Error("Parent not found");
      content.derivative = parent.author != content.author;
    }
  } catch(ex) {
    console.warn("Failed to find parent: ", ex);
    throw "parent not found. Please re-check the variant ID.";
  }

  log("Importing...");
  const { tpse, warnings } = await importFile(file, content, log).catch(ex => {
    console.warn("Import failed: ", ex);
    log("Import failed: " + ex);
    return { tpse: false, warnings: ["Import failed: " + ex] };
  });

  let components = [
    content.type,
    content.author,
    args.id
      ? args.id.replace(/[^\w-]/g, '')
      : file.name.replace(/\.\w+$/, '')
  ].map(el => {
    let comp = sanitizeFilename(el.replace(/\W/g, '_'));
    if (comp == "") throw new Error("Invalid string in ID");
    return comp;
  });
  let ext = /\.\w+$/.exec(file.name)[0];
  content.id = components.join('-');
  content.path = ['content', ...components].join('/') + ext;
  content.tpsePath = ['tpsefiles', ...components].join('/') + ext + '.tpse';
  if (previewName) {
    let preExt = /\.\w+$/.exec(previewName);
    content.previewPath = ['content', ...components].join('/') + ext + '.preview' + preExt;
  }

  log("Finished");
  return { content, tpse, warnings };
}
