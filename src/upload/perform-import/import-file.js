const tetrioplus = require('tetrioplus');

let queue = [];
let wakeup = () => null;
async function importFile() {
  if (queue.length == 0) return;
  let { file, content, logger, resolve, reject } = queue.shift();
  try {
    const tpse = {}
    const storage = {
      get() { return tpse },
      set(val) { Object.assign(tpse, val) },
      remove(arg) {
        let keys = Array.isArray(arg) ? arg : [arg];
        for (let key of keys)
          delete tpse[key];
      }
    };
    const options = {
      ...content.import,
      log(...msg) {
        if (logger) logger(msg.join(' '));
        console.log(...msg);
      }
    };

    let importResult = await tetrioplus.automatic([ file ], storage, options);
    while (importResult.type == 'multi' && importResult.results.length == 1)
      importResult = importResult.results[0];

    let extraLogs = [];

    switch (importResult.type) {
      case 'skin':
        content.type = 'skin';
        content.format = importResult.format;
        content.modified = null;
        break;
      case 'sfx':
        content.type = 'sfx';
        content.format = 'tetrio';
        content.modified = importResult.modified;
        extraLogs.push(...importResult.warnings);
        break;
      default:
        content.type = 'unknown';
        content.format = null;
        content.modified = null;
        break;
    }

    resolve({ tpse, extraLogs });
  } catch(ex) {
    reject(ex);
  }
}

(async () => {
  while (true) {
    while (queue.length) await importFile();
    await new Promise(res => wakeup = res);
  }
})();

let runningImport = null;
module.exports = async function(file, content, logger) {
  return new Promise((resolve, reject) => {
    queue.push({ file, content, logger, resolve, reject });
    wakeup();
  });
}
