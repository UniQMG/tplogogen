module.exports = function(string) {
  const args = {};
  let lastArg = null;
  for (let line of string.split('\n')) {
    let res = /^(\S+?):(.*)$/.exec(line);
    if (!res) {
      if (lastArg) args[lastArg] += '\n' + line;
      continue;
    }
    lastArg = res[1].toLowerCase();
    args[lastArg] = res[2];
  }
  for (let key of Object.keys(args))
    args[key] = args[key].trim();
  return args;
}
