const embeds = require('./embeds');
const mkdirp = require('mkdirp');
const fetch = require('node-fetch');
const fs = require('fs');
const fsp = require('fs/promises');
const zip = require('jszip');
const bufToFile = require('./perform-import/buffer-to-file');

module.exports.makeLogger = function makeLogger(msg) {
  let logs = [];
  let editTimeout = null;
  let editPromise = Promise.resolve();
  async function log(arg) {
    logs.push(arg);
    clearTimeout(editTimeout);
    setTimeout(() => {
      if (!editPromise) return; // no more editing allowed
      editPromise = editPromise.then(() => {
        let lines = logs.slice(-10);
        if (logs.length > 10)
          lines.unshift(`(${logs.length-10} more lines)`);
        return msg.edit({ embeds: [embeds.processing(lines.join('\n'))] });
      });
    }, 1000);
  }
  async function stop() {
    let ep = editPromise;
    editPromise = null;
    await ep;
  }
  return { log, stop };
};

module.exports.packageAttachments = async function(attachments, asBuffer) {
  if (attachments.size == 1) {
    let attachment = attachments.first();
    let buffer = await (await fetch(attachment.url)).arrayBuffer();
    if (asBuffer) return buffer;
    return await bufToFile(attachment.name, Buffer.from(buffer));
  } else {
    let zip = new JSZip();
    for (let attachment of attachments.values()) {
      let subzip = new JSZip();
      let buffer = await (await fetch(attachment.url)).arrayBuffer();
      subzip.file(attachment.name, buffer);
      zip.file(
        attachment.name.split('.').slice(0, -1).join('.') + '.zip',
        await subzip.generateAsync({ type: 'nodebuffer' })
      );
    }
    let buffer = await zip.generateAsync({ type: 'nodebuffer' });
    if (asBuffer) return buffer;
    return await bufToFile(attachments.first().name + '.zip', buffer);
  }
}

module.exports.download = async function download(name, url, dlpath) {
  if (url instanceof Buffer || url instanceof ArrayBuffer) {
    await mkdirp(dlpath.replace(/\\/g, '/').split('/').slice(0, -1).join('/'));
    await fsp.writeFile(dlpath, Buffer.from(url));
    return;
  }
  let response = await fetch(url);
  if (!response.ok) throw new Error(`${name} request failed.`);
  await mkdirp(dlpath.replace(/\\/g, '/').split('/').slice(0, -1).join('/'));
  await new Promise((res, rej) => {
    response.body.on('error', ex => rej(new Error(`${name} request error.`, ex)));
    response.body.on('end', res);
    response.body.pipe(fs.createWriteStream(dlpath));
  });
};

module.exports.get = function get(db, key) {
  if (!key) throw new Error("Bad key");
  return db.get(key).then(val => {
    return JSON.parse(val);
  }).catch(ex => {
    if (ex.notFound) return null;
    throw ex;
  });
};
