const { yhf } = require('../env.json');
const fsp = require('fs/promises');
const level = require('level');
const path = require('path');

// Probably need a proper database for this
const dbvotes = level('./db/votes');
const dbtokens = level('./db/tokens');
const dbusers = level('./db/users');

const ONE_DAY = 24 * 60 * 60 * 1000;
const ONE_WEEK = 7 * ONE_DAY;
const VOTE_PERIOD = ONE_DAY;

async function getUserById(userId) {
  return await dbusers.get(userId).then(v => JSON.parse(v)).catch(async ex => {
    let user = {
      id: userId,
      token: null,
      lastVote: 0
    }
    await dbusers.put(userId, JSON.stringify(user));
    return user;
  });
}
async function issueToken(userId) {
  let user = await getUserById(userId);
  if (user.token) await dbtokens.del(user.token);
  const gen = {
    '%': () => 'ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890'[~~(Math.random()*37)],
    '-': () => '-'
  }
  user.token = [...'%%%%-%%%%-%%%%-%%%%'].map(el => gen[el]()).join('');
  await dbtokens.put(user.token, JSON.stringify({ user: userId }));
  await dbusers.put(userId, JSON.stringify(user));
  return user.token;
}
async function getUser(tokenstr) {
  let token = await dbtokens.get(tokenstr)
    .then(v => JSON.parse(v))
    .catch(ex => null);
  if (!token) return null;
  return JSON.parse(await dbusers.get(token.user));
}
async function getVotes(contentId) {
  let votes = await dbvotes.get(contentId)
    .then(vote => JSON.parse(vote))
    .catch(async ex => {
      let votes = { total: 0, active: [] }
      await dbvotes.put(contentId, JSON.stringify(votes));
      return votes;
    });

  let filtered = false;
  votes.active = votes.active.filter(el => {
    if (Date.now() - el.date > ONE_WEEK) {
      filtered = true;
      return false;
    }
    return true;
  });
  if (filtered) await dbvotes.put(contentId, JSON.stringify(votes));

  return votes;
}
async function vote(token, contentId) {
  let user = await getUser(token);
  if (!user)
    return { ok: false, error: "Token not found" };

  if (Math.floor(Date.now() / VOTE_PERIOD) == Math.floor(user.lastVote / VOTE_PERIOD))
    return { ok: false, error: "Already voted today" };

  user.lastVote = Date.now();
  await dbusers.put(user.id, JSON.stringify(user));

  let votes = await getVotes(contentId);
  votes.total += 1;
  votes.active.push({ date: Date.now() });
  await dbvotes.put(contentId, JSON.stringify(votes));

  await recountVotes();
  return { ok: true };
}
async function recountVotes() {
  let votes = {};
  await new Promise((res, rej) => {
    dbvotes.createReadStream()
      .on('error', rej)
      .on('end', res)
      .on('data', ({ key, value }) => {
        let data = JSON.parse(value);
        votes[key] = {
          total: data.total,
          active: data.active.reduce((sum, vote) => {
            return Date.now() - vote.date < ONE_WEEK ? sum + 1 : sum;
          }, 0)
        };
      });
  });
  await fsp.writeFile(
    path.join(yhf.data_dir, 'votes.json'),
    JSON.stringify(votes)
  );
}

module.exports = { getUserById, getUser, issueToken, vote, getVotes, recountVotes }
