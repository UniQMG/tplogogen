const level = require('level');

const { issueToken } = require('../votes.js');

module.exports = async function(msg) {
  let token = await issueToken(msg.author.id);
  msg.author.send(
    `Token: \`${token}\`. Don't share this or enter it on any site ` +
    `other than https://you.have.fail/ed/at/tetrioplus`
  );
  return "Token issued, check your DMs.";
}
