const rebuildIndex = require('../upload/regenerate-index.js');
const { yhf } = require('../../env.json');
const fsp = require('fs/promises');
const { getIndex } = rebuildIndex;
const path = require('path');

module.exports = async function(msg, ...args) {
  switch (args[0]) {
    case 'list': {
      let tags = new Map();
      for (let content of await getIndex())
        for (let tag of content.tags)
          tags.set(tag, (tags.get(tag) ?? 0)+1);

      let desc = [];
      for (let [tag, count] of tags.entries())
        desc.push(`\`${tag}\` (${count})`);

      msg.reply({embeds: [{
        title: `Total tags: ${desc.length}`,
        description: desc.join(' '),
        color: 0x635293
      }]});
      return false;
    }

    case 'search': {
      let applicable = (await getIndex()).filter(content => {
        return args.slice(1).every(tag => content.tags.includes(tag));
      });
      let desc = applicable.map(content => {
        let title = `${content.author}: ${content.name}`.replace(/`/g, '');
        let link = `https://you.have.fail/ed/at/tetrioplus/#${content.id}`;
        return `[${title}](${link})`;
      });
      if (desc.length > 20) {
        desc.splice(20);
        desc.push('...');
      }

      let skin = desc.length == 1 ? 'skin' : 'skins';
      msg.reply({embeds: [{
        title: `Found ${applicable.length} ${skin} matching ${args.length-1} tags.`,
        description: desc.join('\n'),
        color: 0x635293
      }]});
      return false;
    }

    case 'remove':
      var invert = true;
      // fallthrough

    case 'add': {
      if (!msg.member.roles.resolve(yhf.reviewer))
        throw 'reviewer only command';
      let content = (await getIndex()).filter(ct => ct.id == args[1])[0];
      if (!content) throw `Unknown content ID \`${args[1]}\``;
      let modTags = args.slice(2).map(tag => {
        let negative = tag.startsWith('-');
        if (negative) tag = tag.slice(1);
        if (invert) negative = !negative;
        return { tag: tag.replace(/\W/g, ''), positive: !negative };
      });

      let tags = new Set(content.tags);
      for (let { tag, positive } of modTags)
        positive ? tags.add(tag) : tags.delete(tag);
      content.tags = [...tags];

      await fsp.writeFile(path.join(yhf.data_dir, content.path + '.json'), JSON.stringify(content, null, 2));
      await rebuildIndex();
      // fall through
    }

    case 'show': {
      let content = (await getIndex()).filter(ct => ct.id == args[1])[0];
      if (!content) throw `Unknown content ID \`${args[1]}\``;
      msg.reply({embeds: [{
        title: `${content.author}: ${content.name} has ${content.tags.length} tags`,
        description: content.tags.map(tag => '`' + tag + '`').join(' '),
        color: 0x635293
      }]});
      return false;
    }

    default:
      throw `Unknown sub-command \`${args[0]}\``;
  }
}
