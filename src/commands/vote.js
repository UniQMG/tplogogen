const { getIndex } = require('../upload/regenerate-index');
const votes = require('../votes');

module.exports = async function vote(msg, ...args) {
  let user = await votes.getUserById(msg.author.id);
  let token = user.token ? user.token : await votes.issueToken(msg.author.id);

  let query = args.join(' ');
  let index = await getIndex();
  let tgtId = index.filter(target => target.id == query)[0];
  let tgtName = index.filter(target => target.name == query)[0];
  let target = tgtId || tgtName;
  if (!target) throw "Unknown content, please re-check the ID or name."

  let result = await votes.vote(token, target.id);
  if (!result.ok) throw result.error;
  return "Vote cast";
}
