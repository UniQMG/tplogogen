const { yhf } = require('../../env.json');
const upload = require('../upload');

module.exports = async function(msg, ...args) {
  if (!msg.member.roles.resolve(yhf.reviewer))
    throw 'reviewer only command';
  if (args.length == 0)
    throw `No message IDs specified`;
  for (let msgId of args) {
    if (!/^\d+$/.test(msgId)) {
      throw `Not a message ID: ${msgId}`
      return;
    }
    let submsg = await msg.channel.messages.fetch(msgId);
    if (!submsg) {
      throw `Unknown message ID: #${msgId}`;
      return;
    }
    await upload.delete(submsg);
    if (!await upload.message(submsg))
      throw `Message #${msgId} is not importable`;
  }
  return false;
}
