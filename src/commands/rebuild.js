const regenerateIndex = require('../upload/regenerate-index');
const { admin } = require('../../env.json');

module.exports = async function rebuild(msg, ...args) {
  if (msg.author.id != admin) return false;
  await regenerateIndex();
  return `Finished`;
}
