const { getIndex } = require('../upload/regenerate-index');
const { port } = require('../../env.json');
const express = require('express');
const votes = require('../votes');
const app = express();

app.post('/api/vote/:content', async (req, res) => {
  let index = await getIndex();
  let target = index.filter(target => target.id == req.params.content)[0];
  if (!target) return res.sendStatus(404);

  let result = await votes.vote(req.query.token, target.id);
  if (!result.ok) return res.status(400).send(result.error);

  res.sendStatus(200);
});

app.listen(port);
