const { prefix, token, guild: guildid, admin, yhf } = require('../env.json');
const mkdirp = require('mkdirp');
const path = require('path');

process.on('uncaughtException', ex => {
  console.error("Uncaught", ex);
  process.exit(1);
});
process.on('unhandledRejection', ex => {
  console.error("Unhandled", ex);
});

mkdirp.sync(yhf.data_dir);
mkdirp.sync(path.join(yhf.data_dir, 'content'));
mkdirp.sync(path.join(yhf.data_dir, 'tpsefiles'));
mkdirp.sync('db');

const { recountVotes } = require('./votes');
const generateIcon = require('./generate');
const userupload = require('./upload');
const Discord = require('discord.js');
require('./api/app');

const ONE_DAY = 24 * 60 * 60 * 1000;

const client = new Discord.Client({
  partials: ['MESSAGE', 'CHANNEL', 'REACTION'],
  intents: 0b111111111111111
});

const commands = {
  tag: require('./commands/tag.js'),
  link: require('./commands/link.js'),
  vote: require('./commands/vote.js'),
  rebuild: require('./commands/rebuild.js'),
  resubmit: require('./commands/resubmit.js'),
  setfile: userupload.setSubmissionAttachmentMessage,
  setpreview: userupload.setSubmissionPreview,
  reopen: userupload.reopenSubmission,
  override: userupload.overrideArguments,
  refresh: userupload.refreshSubmission,
  async generate(msg, ...args) {
    let connected = false;
    if (args[0] == 'connected') {
      connected = true;
      args.splice(0, 1);
    }
    await msg.reply({ files: [await generateIcon(connected, args)] });
    return false;
  }
};

function log(level, ...things) {
  console.log(`[${new Date().toISOString()}] {${level}}`, ...things);
}

async function updateIcon() {
  log('INFO', 'Updating server icon');
  try {
    let guild = await client.guilds.fetch(guildid);
    await guild.setIcon(await generateIcon(true, []));
    log('INFO', 'Done updating server icon');
  } catch(ex) {
    log('ERROR', 'Failed to update server icon:', ex);
  }
}

client.on('ready', () => {
  log('INFO', `Logged in as ${client.user.tag}`);
  setInterval(() => updateIcon(), ONE_DAY);
  // also recounted every vote, this is just to flush expired votes
  setInterval(() => recountVotes(), ONE_DAY);
  setInterval(async () => {
    console.log("Bumping threads");
    let guild = await client.guilds.fetch(guildid);
    let channel = await guild.channels.fetch(yhf.submissions_channel);
    // await userupload.bumpThreads(channel); // TODO: temporarily disabled
  }, ONE_DAY/2);
});

client.on('messageReactionAdd', async (reaction, user) => {
  if (reaction.partial) await reaction.fetch();
  let member = await (await client.guilds.fetch(guildid)).members.fetch(user.id);
  await userupload.reaction(reaction.message, reaction, member);
});
client.on('messageDelete', async (msg) => {
  await userupload.delete(msg);
});
client.on('messageUpdate', async (old, msg) => {
  if (!msg.guild) return;
  if (msg.partial) await msg.fetch();
  let messageChannel = msg.channel.isThread()
    ? msg.channel.parent
    : msg.channelId;
  if (messageChannel == yhf.submissions_channel)
    if (await userupload.message(msg))
      return;
  await userupload.message(msg);
});
client.on('messageCreate', async msg => {
  if (!msg.guild) return;
  let messageChannel = msg.channel.isThread()
    ? msg.channel.parent
    : msg.channelId;
  if (messageChannel == yhf.submissions_channel)
    if (await userupload.message(msg))
      return;

  let [command, ...args] = msg.content.trim().split(/\s+/);
  if (!command.startsWith(prefix)) return;
  command = command.slice(prefix.length);

  let executor = commands[command];
  if (!executor) {
    await msg.channel.send('\\❓ Unknown command');
    return;
  }
  try {
    let res = await executor(msg, ...args);
    if (res === false) return;
    await msg.channel.send('\\✔️ ' + res);
  } catch(ex) {
    await msg.channel.send('\\❌ ' + ex.toString());
    if (ex instanceof Error)
      console.log(ex);
  }
});

client.login(token);
