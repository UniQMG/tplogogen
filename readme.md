# TETR.IO PLUS bot

Handles submissions for the site.

Node v16.13.0 recommended

## Setup
- Run `yarn install --ignore-engines`[^1]
- Fill out `./assets/env.json.template` and copy to `./env.json`.
  - Data directory should be set somewhere and then symlinked into the site     
    directory as `data`.
- Run `src/index.js`

[^1]: fluent-ffmpeg `2.1.3` is not compatible with node v`16.13.0`, but previous versions are broken due to [fluent-ffmpeg #1266](https://github.com/fluent-ffmpeg/node-fluent-ffmpeg/issues/1266). `2.1.3` seems to work well enough, though.