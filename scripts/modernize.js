// Script for updating old format content
// Most likely not up to date, this is kept for posterity
// Put old content in './oldcontent', get new content in './$DATA_DIR/content'
const sanitizeFilename = require('sanitize-filename');
const { promisify: pr } = require('util');
const { yhf } = require('../env.json');
const fsp = require('fs/promises');
const mkdirp = require('mkdirp');
const glob = require('glob');

function raise(err) { throw new Error(err); }

(async () => {
  let files = [
    ...await pr(glob)('oldcontent/skins/**/*.*'),
    ...await pr(glob)('oldcontent/soundeffects/**/*.*')
  ];
  files = files.filter(file => !file.endsWith('.json'));

  for (let file of files) {
    let [_oldcontent, type, author, filename] = file.split('/');
    if (!filename) continue; // 'content/skins/aznguy.mp4' gets caught by the glob
    let json = await fsp.readFile(file + '.json')
      .then(v => JSON.parse(v))
      .catch(ex => ({}));

    let content = {
      id: null,
      name: json.title || json.displayname || filename.split('.').slice(0,-1).join('.'),
      author: author,
      description: json.description || "",
      visibility: 'public',
      quality: 'normal',
      uploaded: json.uploaded || 0,
      updated: json.updated || json.uploaded || 0,
      parent: json.variantof || null,
      derivative: false,
      type: null,
      format: null,
      modified: null,
      path: null,
      tpsePath: null,
      source: json.proof || null,
      import: json.import || {}
    };

    switch (type) {
      case 'skins':
        content.type = 'skin'; // no s
        content.format = {
          svg: 'tetriosvg',
          png: 'tetrioraster',
          gif: 'tetrioanim'
        }[filename.split('.').slice(-1)[0]];
        content.modified = null;
        if (!content.format) throw new Error('unknown format');
        break;

      case 'soundeffects':
        content.type = 'sfx';
        if (filename.endsWith('tpse')) {
          content.format = 'tpse'; // kinda ew but some older sfx was tpse only
          content.modified = [];
        } else {
          content.format = 'tetrio';
          content.modified = [];
        }
        break;
    }

    let components = [
      content.type,
      content.author,
      filename
    ].map(el => {
      let comp = sanitizeFilename(el.replace(/[\s\-]/g, '_'));
      if (comp == "") throw new Error("Invalid string in ID");
      return comp;
    });
    content.id = components.join('-').replace(/\.\w+$/, '');
    content.path = `content/${content.type}/${content.author}/${filename}`;

    let outpath = `${yhf.data_dir}/content/${content.type}/${content.author}/${filename}`;
    await mkdirp(outpath.split('/').slice(0, -1).join('/'));
    await fsp.copyFile(file, outpath);
    await fsp.writeFile(outpath + '.json', JSON.stringify(content, null, 2));
  }
})();
