// Script for doing various migrations
// Run this AFTER modernize
// Updates json files in-place
const { promisify: pr } = require('util');
const fsp = require('fs/promises');
const glob = require('glob');
const rebuildIndex = require('../src/upload/regenerate-index');

(async () => {
  let files = await pr(glob)('content/content/**/*.json');

  let migrations = [
    data => data.tags = []
  ];

  for (let file of files) {
    let data = JSON.parse(await fsp.readFile(file, 'utf8'));
    let version = data.version ?? 0;

    for (let i = version; i < migrations.length; i++) {
      await migrations[i](data);
      data.version = i+1;
      console.log(`${data.id} @ v${data.version}`);
    }

    await fsp.writeFile(file, JSON.stringify(data, null, 2));
  }
  console.log('Rebuilding index...');
  await rebuildIndex();
  console.log('Done');
})();
