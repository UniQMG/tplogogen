const bufToFile = require('../src/upload/perform-import/buffer-to-file');
const importFile = require('../src/upload/perform-import/import-file');
const sanitizeFilename = require('sanitize-filename');
const { promisify: pr } = require('util');
const { yhf } = require('../env.json');
const fsp = require('fs/promises');
const mkdirp = require('mkdirp');
const glob = require('glob');
const path = require('path');

process.on('uncaughtException', ex => { console.error(ex); process.exit(1); });
process.on('unhandledRejection', ex => { console.error(ex); process.exit(1); });

(async () => {
  let files = await pr(glob)(`${yhf.data_dir}/content/**/*.json`);
  files = files.sort((a,b) => a.indexOf('sfx') > b.indexOf('sfx') ? 1 : -1);

  let [node, scriptPath, ...args] = process.argv;

  for (const filepath of files) {
    const contentpath = filepath.slice(0, -'.json'.length);
    const content = JSON.parse(await fsp.readFile(filepath, 'utf8'));
    if (args.length > 0 && !args.includes(content.id)) continue;

    console.log(`${filepath}...`);
    const file = await bufToFile(
      contentpath.split('/').slice(-1)[0],
      await fsp.readFile(contentpath)
    );

    try {
      let components = [
        content.type,
        content.author,
        file.name
      ].map(el => {
        let comp = sanitizeFilename(el.replace(/[\s\-]/g, '_'));
        if (comp == "") throw new Error("Invalid string in ID");
        return comp;
      });
      content.tpsePath = ['tpsefiles', ...components].join('/') + '.tpse';
      const { tpse } = await importFile(file, content, () => null);
      if (!tpse) {
        console.log(`${filepath}: FAILED`);
        continue;
      }
      await mkdirp(path.join(yhf.data_dir, content.tpsePath).split('/').slice(0, -1).join('/'));
      await fsp.writeFile(filepath, JSON.stringify(content, null, 2));
      await fsp.writeFile(path.join(yhf.data_dir, content.tpsePath), JSON.stringify(tpse));
      console.log(`${filepath}: ok`);
    } catch(ex) {
      console.log(`${filepath}: ERRORED (${ex})`);
    }

    if (args.length > 0 && args.includes(content.id)) {
      args.splice(args.indexOf(content.id), 1);
      if (args.length == 0) break;
    }
  }

  for (let arg of args) {
    console.error(`Unknown content '${arg}'`);
  }
})();
