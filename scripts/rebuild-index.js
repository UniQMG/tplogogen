const rebuildIndex = require('../src/upload/regenerate-index');
const { recountVotes } = require('../src/votes');
rebuildIndex().then(() => recountVotes()).catch(ex => {
  console.error(ex);
  process.exit(1);
});
